/**
 * 
 */
package org.lshl.swing.text;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author lshl
 *
 * Created 15/10/2017
 */
public class RegexFormatterTest {

    /**
     * @throws java.lang.Exception Not thrown
     */
    @SuppressWarnings("unused")
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        //empty
    }

    /**
     * @throws java.lang.Exception Not thrown
     */
    @SuppressWarnings("unused")
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        //empty
    }

    /**
     * @throws java.lang.Exception Not thrown
     */
    @SuppressWarnings("unused")
    @Before
    public void setUp() throws Exception {
        //empty
    }

    /**
     * @throws java.lang.Exception Not thrown
     */
    @SuppressWarnings("unused")
    @After
    public void tearDown() throws Exception {
        //empty
    }

    /**
     * Test method for {@link org.lshl.swing.text.RegexFormatter#getPattern()}.
     */
    @SuppressWarnings("static-method")
    @Test
    public void testGetPattern() {
        String  pat = "(ABC){1,3}";
        Pattern p   = Pattern.compile(pat);
        RegexFormatter rf = new RegexFormatter(pat);
        assertEquals(p.pattern(), rf.getPattern().pattern());
    }

    /**
     * Test method for {@link org.lshl.swing.text.RegexFormatter#stringToValue(java.lang.String)}.
     */
    @SuppressWarnings("static-method")
    @Test
    public void testStringToValueString() {
        RegexFormatter rf = new RegexFormatter("(\\d+)(,(\\d+))*");
        String s = "16,20";
        Object o = null;
        try {
            o = rf.stringToValue(s);
        } catch (ParseException e) {
            fail("Parse failed("+e+")");
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        assertEquals(s, o);
        s = "16-20";
        try {
            o = rf.stringToValue(s);
            fail("parse supposed to fail '"+s+"'");
        } catch (ParseException e) {
            assertTrue("Parse failed("+e+")", true);
        }
    }

}
