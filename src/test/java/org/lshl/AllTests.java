package org.lshl;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.lshl.swing.text.RegexFormatterTest;

/**
 * @author Shlomi Levi
 */
@RunWith(Suite.class)
@SuiteClasses({RegexFormatterTest.class})
public class AllTests {
    //no body needed
}
