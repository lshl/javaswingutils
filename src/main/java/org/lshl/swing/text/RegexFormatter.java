package org.lshl.swing.text;

import java.text.ParseException;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.swing.text.DefaultFormatter;

/**
 * @author Shlomi Levi
 *
 * Created 15/10/2017
 */
public class RegexFormatter extends DefaultFormatter {
    private static final long serialVersionUID = 802367404253315800L;

    private static Logger logger = Logger.getLogger(RegexFormatter.class.getName());

    private Pattern pattern;
    private Matcher matcher;
    /**
     * Ctor.
     *
     * @param  regex  The regular expression to validate by.
     *
     * @throws  PatternSyntaxException  If the expression's syntax is invalid.
     *
     * @see Pattern#compile(String)
     */
    public RegexFormatter(String regex) throws PatternSyntaxException {
        super();
        pattern = Pattern.compile(regex);
        matcher = null;
    }

    /**
     * Gets the acceptance pattern for this formatter.
     *
     * @return The acceptance pattern for this formatter.
     */
    public Pattern getPattern() {
        return pattern;
    }

    /**
     * @see javax.swing.text.DefaultFormatter#stringToValue(java.lang.String)
     */
    @Override
    public Object stringToValue(String text) throws ParseException {
        logger.entering(RegexFormatter.class.getName(), "stringToValue", text);
        matcher = pattern.matcher(text);
        if (matcher.matches()) {
            Object o = super.stringToValue(text);
            logger.exiting(RegexFormatter.class.getName(), "stringToValue", o);
            return o;
        }
        throw new ParseException("Pattern did not match", 0);
    }
}
