package org.lshl;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A version of the library.
 *
 * Created 5 Feb 2017
 */
public final class JavaSwingUtilsVersion {
    /**
     * Library's name.
     */
    public static final String LIB_NAME       = "JavaSwingUtils";
    /**
     * Library's description.
     */
    public static final String LIB_DESCRPT    = "Java Swing auxiliary and utilities";
    /**
     * Library's version major part.
     */
    public static final int    MAJOR          = 1;
    /**
     * Library's version minor part.
     */
    public static final int    MINOR          = 1;
    /**
     * Library's version build number part.
     */
    public static final int    BUILD_NUMBER   = 6;
    /**
     * Library's version string.
     */
    public static final String VERSION        = ""+MAJOR+"."+MINOR+"."+BUILD_NUMBER;
    /**
     * Library's build date.
     */
    public static final Date   BUILD_DATE     = new Date(1508065141271L);
    /**
     * Library's build date string.
     */
    public static final String BUILD_DATE_STR = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z (EEE, dd MMM yyyy)").format(BUILD_DATE);

    /**
     * Non-instantiable class.
     */
    private JavaSwingUtilsVersion() {throw new UnsupportedOperationException();}

    /**
     * A full dump of the library's information.
     *
     * @return A string containing the library information.
     */
    public static String fullVersionStr() {
        StringWriter sw = new StringWriter();
        BufferedWriter bw = new BufferedWriter(sw);
        try {
            bw.write(LIB_NAME);       bw.newLine();
            bw.write(LIB_DESCRPT);    bw.newLine();
            bw.write(VERSION);        bw.newLine();
            bw.write(BUILD_DATE_STR);
            bw.close();
            sw.close();
        } catch (IOException e) {
            throw new RuntimeException("This was not supposed to happen!", e);
        }
        return sw.toString();
    }

    /**
     * Prints the version to {@link System#out}.
     *
     * @see #fullVersionStr()
     */
    public static void printVersion() {
        System.out.println(fullVersionStr());
    }

    /**
     * Prints library information.
     * @param  args  Not used.
     */
    public static void main(String[] args) {
        printVersion();
    }
}
